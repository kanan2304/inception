package az.ingress.ms14.service;

import az.ingress.ms14.dto.MarketRequest;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.dto.RegionRequest;
import az.ingress.ms14.dto.RegionResponse;
import az.ingress.ms14.model.Branch;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.model.Region;
import az.ingress.ms14.repository.MarketRepository;
import az.ingress.ms14.repository.RegionRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegionService {
    private final RegionRepository regionRepository;
    private final ModelMapper modelMapper;

    public RegionResponse create(RegionRequest regionRequest) {
        Region region = modelMapper.map(regionRequest, Region.class);
        regionRepository.save(region);
        return modelMapper.map(region, RegionResponse.class);
    }

    @Transactional
    public RegionResponse get(Long regionId) {
        Region region = regionRepository.findById(regionId).orElseThrow(() -> new RuntimeException("Region with id" + regionId + " not found"));
        return modelMapper.map(region, RegionResponse.class);
    }
    public void update(Long regionId, RegionRequest regionRequest) {
        Region region = regionRepository.findById(regionId).orElseThrow(() -> new RuntimeException("Market with id" + regionId + " not found"));
        region.setName(regionRequest.getName());
        regionRepository.save(region);
    }
    public void delete(Long regionId) {
        Region region = regionRepository.findById(regionId).orElseThrow(() -> new RuntimeException("Market id" + regionId + "is not found"));
        regionRepository.deleteById(regionId);
    }
}
