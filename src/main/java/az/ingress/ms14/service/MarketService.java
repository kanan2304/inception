package az.ingress.ms14.service;

import az.ingress.ms14.dto.MarketRequest;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.repository.MarketRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarketService {
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public MarketResponse create(MarketRequest marketRequest) {
        Market market = modelMapper.map(marketRequest, Market.class);
        marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }

    @Transactional
    public MarketResponse get(Long marketId) {
        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException("Market with id" + marketId + " not found"));
        return modelMapper.map(market, MarketResponse.class);
    }
    public void update(Long marketId, MarketRequest marketRequest) {
        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException("Market with id" + marketId + " not found"));
        market.setName(marketRequest.getName());
        market.setType(marketRequest.getType());
        marketRepository.save(market);
    }
//    public void delete(Long marketId) {
//        marketRepository.deleteById(marketId);
//    }
}