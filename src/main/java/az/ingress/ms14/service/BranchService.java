package az.ingress.ms14.service;

import az.ingress.ms14.dto.BranchRequest;
import az.ingress.ms14.dto.BranchResponse;
import az.ingress.ms14.model.Branch;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.repository.AddressRepository;
import az.ingress.ms14.repository.BranchRepository;
import az.ingress.ms14.repository.MarketRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchService {
    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;
    public BranchResponse create(Long marketId, BranchRequest branchRequest) {
        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException("Market with id" + marketId + " not found"));
        Branch branch = modelMapper.map(branchRequest, Branch.class);
        branch.setMarket(market);
        market.getBranches().add(branch);
        branchRepository.save(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }

    public BranchResponse update(Long marketId, Long branchId, BranchRequest branchRequest) {
        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException("Market id" + marketId + "is not found"));
        Branch branch = branchRepository.findById(branchId).orElseThrow(() -> new RuntimeException("Branch id" + branchId + "is not found"));
        branch.setName(branchRequest.getName());
        branch.setCountOfEmployee(branchRequest.getCountOfEmployee());
        return modelMapper.map(branch, BranchResponse.class);
    }

    public void delete(Long marketId, Long branchId) {
        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException("Market id" + marketId + "is not found"));
        Branch branch = branchRepository.findById(branchId).orElseThrow(() -> new RuntimeException("Branch id" + branchId + "is not found"));
        branchRepository.deleteById(branchId);
    }

    @Transactional
    public BranchResponse get(Long marketId, Long branchId) {
        Market market = marketRepository.findById(marketId).orElseThrow(() -> new RuntimeException("Market id" + marketId + "is not found"));
        Branch branch = branchRepository.findById(branchId).orElseThrow(() -> new RuntimeException("Market id" + branchId + "is not found"));
        return modelMapper.map(branch, BranchResponse.class);
    }
}