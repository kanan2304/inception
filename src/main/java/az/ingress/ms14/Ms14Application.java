package az.ingress.ms14;

import az.ingress.ms14.model.Address;
import az.ingress.ms14.model.Branch;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.model.Region;
import az.ingress.ms14.repository.AddressRepository;
import az.ingress.ms14.repository.BranchRepository;
import az.ingress.ms14.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
@RequiredArgsConstructor
public class Ms14Application implements CommandLineRunner {
    private final MarketRepository marketRepository;
    private final BranchRepository branchRepository;
    private final AddressRepository addressRepository;
    public static void main(String[] args) {
        SpringApplication.run(Ms14Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
//        Market fresco = marketRepository.findById(3L).get();
//        Market bravo = marketRepository.findById(2L).get();
//
//        Region west = Region.builder()
//                .name("West")
//                .build();
//        Region asia = Region.builder()
//                .name("Asia")
//                .build();
//
//
//        fresco.getRegions().add(west);
//        fresco.getRegions().add(west);
//        bravo.getRegions().add(west);
//        bravo.getRegions().add(west);
//
//        marketRepository.save(bravo);
//        marketRepository.save(fresco);


//        Market market = Market.builder()
//                .name("Fresco")
//                .type("Hyper")
//                .build();
////        Market market = Market.builder()
////                .name("Walmart")
////                .type("Super")
////                .build();
//
//        Branch alabama = Branch.builder()
//                .name("West Virginia")
//                .countOfEmployee(500)
//                .market(market)
//                .build();
//        Branch washingtonDc = Branch.builder()
//                .name("Florida")
//                .market(market)
//                .countOfEmployee(250)
//                .build();
//
//        market.getBranches().add(alabama);
//        market.getBranches().add(washingtonDc);
//
//
//        Address address1 = Address.builder()
//                .name("321 Local Mall")
//                .build();
//        Address address2 = Address.builder()
//                .name("9878 CountrySide river")
//                .build();
//        alabama.setAddress(address1);
//        washingtonDc.setAddress(address2);
//
//        addressRepository.save(address1);
//        addressRepository.save(address2);
//
//        branchRepository.save(alabama);
//        branchRepository.save(washingtonDc);
//        marketRepository.save(market);



//        marketRepository.findAll().forEach(System.out::println);


//        Student student = studentRepository.findById(2).get();
//        System.out.println(student.getPhoneNumberList());

//        PhoneNumber phone1 = PhoneNumber.builder()
//                .number("+994505005050")
//                .student(student)
//                .build();
//        PhoneNumber phone2 = PhoneNumber.builder()
//                .number("+994506785432")
//                .student(student)
//                .build();
//        phoneNumberRepository.save(phone1);
//        phoneNumberRepository.save(phone2);
//
//        student.getPhoneNumberList().add(phone1);
//        student.getPhoneNumberList().add(phone2);
//        studentRepository.save(student);


//        PhoneNumber phone3 = PhoneNumber.builder()
//                .number("+994502222222")
//                .build();
//        phoneNumberRepository.save(phone3);


        //JDBC
//        Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5455/postgres","postgres","password");
//        Statement statement = con.createStatement();
//        ResultSet resultSet = statement.executeQuery("select * from student");
//        while (resultSet.next()) {
//            System.out.println(resultSet.getInt(1));
//            System.out.println(resultSet.getInt(2));
//            System.out.println(resultSet.getString(3));
//        }


//        EntityManagerFactory emf = Persistence.createEntityManagerFactory("persistenceUnitName");
//        EntityManager entityManager = emf.createEntityManager();
//        entityManager.getTransaction().begin();
//
//        List<Student> resultList = entityManager.createNativeQuery("select * from student", Student.class)
//                .getResultList();
//        System.out.println(resultList);
//
//        entityManager.getTransaction().commit();
//        entityManager.close();
    }
}