package az.ingress.ms14.controller;

import az.ingress.ms14.dto.MarketRequest;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {
    private final MarketService marketService;

    @PostMapping
    public MarketResponse create(@RequestBody MarketRequest marketRequest) {
        return marketService.create(marketRequest);
    }

    @GetMapping("/{marketId}")
    public MarketResponse get(@PathVariable Long marketId) {
        return marketService.get(marketId);
    }
    @PutMapping("/{marketId}")
    public void update(@PathVariable Long marketId, @RequestBody MarketRequest marketRequest){
        marketService.update(marketId, marketRequest);
    }
//    @DeleteMapping("/{marketId")
//    public void delete(@PathVariable Long marketId) {
//        marketService.delete(marketId);
//    }
}