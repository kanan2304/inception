package az.ingress.ms14.controller;

import az.ingress.ms14.dto.BranchRequest;
import az.ingress.ms14.dto.BranchResponse;
import az.ingress.ms14.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class BranchController {
    private final BranchService branchService;
    @GetMapping("/{marketId}/branch/{branchId}")
    public BranchResponse get(@PathVariable Long marketId,@PathVariable Long branchId){
        return branchService.get(marketId,branchId);
    }

    @PostMapping("/{marketId}")
    public BranchResponse create(@PathVariable Long marketId, @RequestBody BranchRequest branchRequest) {
        return branchService.create(marketId, branchRequest);
    }

    @PutMapping("/{marketId}/branch/{branchId}")
        public BranchResponse update(@PathVariable Long marketId,
                                     @PathVariable Long branchId,
                                     @RequestBody BranchRequest branchRequest){
            return branchService.update(marketId, branchId, branchRequest);
        }
    @DeleteMapping("/{marketId}/branch/{branchId}")
        public void delete(@PathVariable Long marketId, @PathVariable Long branchId){
            branchService.delete(marketId, branchId);
        }
    }