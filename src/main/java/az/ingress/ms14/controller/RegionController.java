package az.ingress.ms14.controller;

import az.ingress.ms14.dto.MarketRequest;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.dto.RegionRequest;
import az.ingress.ms14.dto.RegionResponse;
import az.ingress.ms14.service.MarketService;
import az.ingress.ms14.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {
    private final RegionService regionService;

    @PostMapping
    public RegionResponse create(@RequestBody RegionRequest regionRequest) {
        return regionService.create(regionRequest);
    }

    @GetMapping("/{regionId}")
    public RegionResponse get(@PathVariable Long regionId) {
        return regionService.get(regionId);
    }
    @PutMapping("/{regionId}")
    public void update(@PathVariable Long regionId, @RequestBody RegionRequest regionRequest){
        regionService.update(regionId, regionRequest);
    }
    @DeleteMapping("/{regionId}")
    public void delete(@PathVariable Long regionId){
        regionService.delete(regionId);
    }
}
