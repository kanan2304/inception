package az.ingress.ms14.controller;

import az.ingress.ms14.dto.AddressRequest;
import az.ingress.ms14.dto.AddressResponse;
import az.ingress.ms14.model.Address;
import az.ingress.ms14.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;

    @PostMapping("/{branchId}")
    public AddressResponse create(@PathVariable Long branchId, @RequestBody AddressRequest addressRequest) {
        return addressService.create(branchId, addressRequest);
    }
    @PutMapping("{branchId}/address/{addressId}")
    public AddressResponse update(@PathVariable Long branchId,@PathVariable Long addressId, @RequestBody AddressRequest addressRequest) {
        return addressService.update(branchId, addressId, addressRequest);
    }
    @GetMapping("{branchId}")
    public AddressResponse get(@PathVariable Long branchId){
        return addressService.get(branchId);
    }
    @DeleteMapping("{branchId}")
    public void delete(@PathVariable Long branchId){
        addressService.delete(branchId);
    }
}
