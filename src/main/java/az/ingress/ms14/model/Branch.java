package az.ingress.ms14.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "branch")
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    Integer countOfEmployee;
    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    Market market;
    @OneToOne(cascade = CascadeType.ALL)
    Address address;
}
